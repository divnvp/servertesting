package com.company.servertesting;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "UsersDB7.db";
    static final String TABLE_USERS = "Users7";
    public final static String TABLE_CHANGES = "changes7";

    static final String KEY_ID = "id";
    static final String KEY_KEYCODE = "keyCard";
    static final String KEY_BALANCE = "balance";
    static final String COLUMN_MINUS = "minus";
    static final String COLUMN_DATE = "date";
    static final String COLUMN_SYNCH = "synch";
    static final String COLUMN_ID = "userId";

    DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_USERS + "(" + KEY_ID +
                " long primary key," + KEY_KEYCODE + " text," +
                KEY_BALANCE + " integer" + ")");

        db.execSQL("create table " + TABLE_CHANGES + "(" + COLUMN_ID +
                " long primary key," + KEY_ID + " long," + COLUMN_MINUS +
                " long," + COLUMN_DATE + " text," + COLUMN_SYNCH +
                " INTEGER DEFAULT 0" + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_USERS);
        db.execSQL("drop table if exists " + TABLE_CHANGES);
        onCreate(db);
    }
}
