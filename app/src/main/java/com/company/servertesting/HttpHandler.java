package com.company.servertesting;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpHandler{
    private static final String TAG = HttpHandler.class.getSimpleName();
    private OkHttpClient client;
    private ArrayList<HashMap<String, String>> usersList;

    private String cookie = null;

    public HttpHandler() {
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void makeServiceCall(String reqUrl,SQLiteDatabase database) {

        try {
            URL url = new URL(reqUrl);

            client = new OkHttpClient.Builder()
                    .connectTimeout(20000, TimeUnit.MILLISECONDS)
                    .readTimeout(20000, TimeUnit.MILLISECONDS)
                    .build();

            Request request = new Request.Builder()
                    .url(url)
                    .header("X-Content-Type-Options","nosniff")
                    .header("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
                    .header("X-XSS-Protection", "1; mode=block")
                    .header("Pragma", "no-cache")
                    .header("X-Frame-Options","DENY")
                    .header("Transfer-Encoding", "chunked")
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Cookie", cookie)
                    .get()
                    .build();

            try {
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {

                    }

                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        //Log.i(TAG, "GET"+response.body().string());
                        final ContentValues contentValues = new ContentValues();
                        try {
                            usersList = new ArrayList<>();
                            String s = response.body().string();
                            JSONArray array = new JSONArray(s);
                            for(int i = 0; i < array.length(); i++){
                                final JSONObject object1 = array.getJSONObject(i);
                                final String id = object1.getString("id");
                                final String keyCard = object1.getString("keyCard");
                                final String balance = object1.getString("balance");
                                final HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("id", id);
                                hashMap.put("keyCard", keyCard);
                                hashMap.put("balance", balance);
                                usersList.add(hashMap);

                                contentValues.put(DBHelper.KEY_ID, id);
                                contentValues.put(DBHelper.KEY_KEYCODE, keyCard);
                                contentValues.put(DBHelper.KEY_BALANCE, balance);

                                if (id.isEmpty())
                                    database.insert(DBHelper.TABLE_USERS, null, contentValues);
                            }

                            Cursor cursor = database.query(DBHelper.TABLE_USERS, null,null,
                                    null,null,null,null);
                            if(cursor.moveToFirst()){
                                int index = cursor.getColumnIndex(DBHelper.KEY_ID);
                                int keyCodeIndex = cursor.getColumnIndex(DBHelper.KEY_KEYCODE);
                                int balanceIndex = cursor.getColumnIndex(DBHelper.KEY_BALANCE);
                                do {
                                    Log.d("mLog", "ID = "+cursor.getLong(index)+
                                            ", keyCode = " + cursor.getString(keyCodeIndex)+
                                            ", balance = " + cursor.getInt(balanceIndex));
                                }while (cursor.moveToNext());
                            }else
                                Log.d("mLog", "0 rows");
                            cursor.close();
                        } catch (final JSONException e) {
                            Log.e(TAG, "Json parsing error: " + e.getMessage());
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    public void sendData(String auth) throws MalformedURLException{
        URL url = new URL(auth);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(20000, TimeUnit.MILLISECONDS)
                .readTimeout(20000, TimeUnit.MILLISECONDS)
                .build();

        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("login","point")
                .addFormDataPart("pwd","point")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .header("X-Content-Type-Options","nosniff")
                .header("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
                .header("X-XSS-Protection", "1; mode=block")
                .header("Pragma", "no-cache")
                .header("X-Frame-Options","DENY")
                .addHeader("Content-Type", "multipart/form-data")
                .post(body)
                .build();

        try(Response response = client.newCall(request).execute()) {
            Log.i(TAG, response.body().string());
            //Log.i(TAG, response.header("Set-Cookie"));
            cookie = response.header("Set-Cookie");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void postSynchData(String operation, int sum, SQLiteDatabase db,
                              Cursor c, int index){
        try {
            URL url = new URL(operation);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(20000, TimeUnit.MILLISECONDS)
                    .readTimeout(20000, TimeUnit.MILLISECONDS)
                    .build();

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sdf.format(new Date());
            ContentValues cv = new ContentValues();

            if (c.moveToFirst()) {
                do {
                    cv.put(DBHelper.KEY_ID, c.getLong(index));
                    cv.put(DBHelper.COLUMN_MINUS, sum);
                    cv.put(DBHelper.COLUMN_DATE, date);
                    //cv.put(DBHelper.COLUMN_SYNCH, c.getInt(synchIndex)==1);
                    db.insert(DBHelper.TABLE_CHANGES, null, cv);
//                    if (c.getInt(synchIndex)==0){

                    // db.update(DBHelper.TABLE_CHANGES, cv, null, null);
                    //}

                    Log.d("mLog", "ID = " + c.getLong(index) +
                            ", sum = " + c.getInt(c.getColumnIndex(DBHelper.COLUMN_MINUS)) +
                            ", date = " + c.getInt(c.getColumnIndex(DBHelper.COLUMN_DATE)) +
                            ", userID = " + c.getInt(c.getColumnIndex(DBHelper.COLUMN_ID)));
                } while (c.moveToNext());
            }
            c.close();

            RequestBody body = new FormBody.Builder()
                    .add("id", String.valueOf(index))
                    .add("sum", String.valueOf(sum))
                    .add("data", date)
                    .add("userId", String.valueOf(c.getColumnIndex(DBHelper.COLUMN_ID)))
                    .build();

            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("Cookie", cookie)
                    .post(body)
                    .build();

            try(Response response = client.newCall(request).execute()) {
                Log.i(TAG, response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
