package com.company.servertesting;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.android.volley.toolbox.HttpResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


import static com.company.servertesting.App.CHANNEL_ID;

public class ServerService extends android.accessibilityservice.AccessibilityService {

    private static int VENDOR_ID = 0;
    private static int PRODUCT_ID = 0;
    private UsbManager mUsbManager = null;

    private static final int STATE_SUM = 0;
    private static final int STATE_COMMA = 2;
    private static final String APP_PREFERENCES = "mSettings";

    private int currentState = STATE_SUM;
    private int currentSum = 0;

    private int[] keys = {0x27, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26};
    private int enter = 0x28;
    private int esc = 0x29;
    private int comma = 0x37;
    private int backspace = 0x2a;
    private int f1 = 0x3a;

    private ArrayList<Integer> keyList = new ArrayList<>();
    private int commaIndex = 0;
    private int sum = 0;

    private SharedPreferences mSettings;
    String host = null;
    String auth = null;
    String operation = null;
    DBHelper dbHelper;
    String TAG = ServerService.class.getSimpleName();
    ArrayList<HashMap<String, String>> usersList;

    HttpHandler httpHandler = new HttpHandler();
    SyncService syncService = new SyncService(httpHandler);

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {

    }

    @Override
    public void onInterrupt() {

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        return super.onKeyEvent(event);
    }

    public void numberAction(int number) {
        if(currentState == STATE_COMMA) {
            if(commaIndex >= 2) {
                return;
            } else {
                commaIndex++;
            }
        }
        keyList.add(number);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void enterAction(){
        if ( keyList.size()!=10 &&(currentState==STATE_SUM || currentState == STATE_COMMA)){
            //
            currentSum = 0;
            for (int i =0; i < keyList.size(); i++){
                currentSum += Math.pow(10,i)*keyList.get(keyList.size()-1-i);
            }
            currentSum *= Math.pow(10,2-commaIndex);
            sum +=currentSum;
            Log.i(TAG, "Сумма списания = "+sum);
        }

        else {
            StringBuilder key = new StringBuilder();
            for(int i = 0; i < keyList.size(); i++) {
                key.append(keyList.get(i).toString());
            }
            Log.i(TAG, "Номер карты : "+key);

            final SQLiteDatabase db = dbHelper.getWritableDatabase();
            String rawQuery = "SELECT * FROM " + DBHelper.TABLE_USERS + " WHERE keyCard = ?";
            Cursor c = db.rawQuery(rawQuery, new String[]{String.valueOf(key)});

            final int index = c.getColumnIndex(DBHelper.KEY_ID);
            final int keyCodeIndex = c.getColumnIndex(DBHelper.KEY_KEYCODE);
            final int balanceIndex = c.getColumnIndex(DBHelper.KEY_BALANCE);

            if (c.moveToFirst()) {
                do {
                    ContentValues cv = new ContentValues();
                    int cvi =  c.getInt(c.getColumnIndex(DBHelper.KEY_BALANCE)) - sum;
                    cv.put(DBHelper.KEY_BALANCE, cvi);
                    // тут как понимаю происходит обновление баланса, но я не вижу создание записи
                    // в БД о проведенной операции
                    /*
                        Нашел!) он в том месте где ведется работа с запросом. Надо стараться не перемешивать код.
                        Если класс работает с сетью пусть так и будет, я создал SyncService там будет логика синхронизации
                     */

                    db.update(DBHelper.TABLE_USERS, cv, "keyCard = ?", new String[]{String.valueOf(key)});
                    Log.d("mLog", "ID = " + c.getLong(index) +
                            ", keyCode = " + c.getString(keyCodeIndex) +
                            ", balance = " + c.getInt(balanceIndex));

                    try {
//                        Timer timer = new Timer();
//                        TimerTask timerTask = new TimerTask() {
//                            @Override
//                            public void run() {
                        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
                        operation = mSettings.getString("operation", "");
                        // сразу после проведения операции отправку можно не делать
                        // просто записываем в БД операцию  с пометкой, что нет синхронизации
                        httpHandler.postSynchData(operation, sum, db, c, index);
//                            }
//                        };
//                        timer.schedule(timerTask, 0,15000);
                    }catch (IllegalStateException e){
                        e.printStackTrace();
                    }

//                    cv.put(DBHelper.KEY_ID, c.getLong(c.getColumnIndex(DBHelper.KEY_ID)));
//                    cv.put(DBHelper.KEY_KEYCODE, c.getString(c.getColumnIndex(DBHelper.KEY_KEYCODE)));
//                    cv.put(DBHelper.COLUMN_MINUS, sum);
//                    db.insert(DBHelper.TABLE_CHANGES, null, cv);
                } while (c.moveToNext());
            }
            c.close();

            keyList.clear();

            escAction();
        }
        keyList.clear();
        currentSum = 0;
        commaIndex = 0;
        currentState = STATE_SUM;
    }

    public void commaAction() {
        if(currentState == STATE_SUM) {
            currentState = STATE_COMMA;
        }
    }

    public void backspaceAction(){
        if(currentState==STATE_SUM){
            keyList.remove(keyList.size()-1);
        }
        else if(currentState==STATE_COMMA){
            commaIndex=0;
            keyList.remove(keyList.size()-1);
        }
    }

    public void escAction() {
        Log.i(TAG, "Введите сумму списания");
        keyList.clear();
        currentState = STATE_SUM;
        sum = 0;
        currentSum = 0;
        commaIndex = 0;
    }

    void saveSettings(){
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = mSettings.edit();
        ed.putString("localhost", "http://192.168.0.14:9009/users");
        ed.putInt("vid", 2303);
        ed.putInt("pid", 9);
        ed.putString("auth", "http://192.168.0.14:9009/auth");
        ed.putString("operation", "http://192.168.0.14:9009/users/operation");
        ed.apply();
        Log.i(TAG, "Settings saved!");
        Log.i(TAG, "--------------");
        Log.i(TAG, mSettings.getString("localhost", ""));
        Log.i(TAG, String.valueOf(mSettings.getInt("vid", 0)));
        Log.i(TAG, String.valueOf(mSettings.getInt("pid", 0)));
        Log.i(TAG, mSettings.getString("auth", ""));
        Log.i(TAG, mSettings.getString("operation", ""));
        Log.i(TAG, "--------------");
        File mSettingsF = new File(Environment.getExternalStorageDirectory().toString());
        File mSettingsFile = new File(mSettingsF, "SharedPreference");
        try {
            FileWriter fileWriter = new FileWriter(mSettingsFile);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            Map<String,?> prefsMap = mSettings.getAll();
            for (Map.Entry<String,?> entry : prefsMap.entrySet()){
                printWriter.println(entry.getKey() + ": " + entry.getValue().toString());
            }
            printWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            Log.i(TAG, e.toString());
        }
    }

    public class authA extends AsyncTask<Void, Void, Void>{

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
        @Override
        protected Void doInBackground(Void... voids) {
            mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
            auth = mSettings.getString("auth","");
            try {
                httpHandler.sendData(auth);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate() {

//        escAction();
        super.onCreate();
        saveSettings();

        usersList = new ArrayList<>();
        dbHelper = new DBHelper(this);
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        host = mSettings.getString("localhost","");
        VENDOR_ID = mSettings.getInt("vid",0);
        PRODUCT_ID = mSettings.getInt("pid",0);

        Log.i(TAG, "FIND:  " + VENDOR_ID + " " + PRODUCT_ID);

        new authA().execute();
        new GetUsers().execute();
        //new PostSynchronized().execute();

        UsbDevice device;
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

        syncService.start();

        while (deviceIterator.hasNext()) {
            device = deviceIterator.next();
            Log.i(TAG, "~~~~~~~~ " + device.getVendorId() + " " + device.getProductId());
            try {
                if (device.getVendorId() == VENDOR_ID && device.getProductId() == PRODUCT_ID) {
                    final HidDevice hidDevice = new HidDevice(getApplicationContext(), device);
                    final byte[] buf = new byte[8];

                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            while (true) {
                                try {
                                    int l = 0;
                                    l = hidDevice.read(buf, buf.length);
                                    for (int i = 0; i < l; i++) {
                                        if (buf[i] == backspace) {
                                            backspaceAction();
                                        } else if (buf[i] == comma) {
                                            commaAction();
                                        } else if (buf[i] == enter) {
                                            enterAction();
                                        } else if (buf[i] == esc) {
                                            escAction();
                                        } else {
                                            for (int j = 0; j < keys.length; j++) {
                                                if (buf[i] == keys[j]) {
                                                    Log.i(TAG, "- " + j);
                                                    numberAction(j);
                                                }
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    }).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public class GetUsers extends AsyncTask<Void, Void, Void> {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... voids) {
            mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
            host = mSettings.getString("localhost", "");
            final SQLiteDatabase database = dbHelper.getWritableDatabase();
            httpHandler.makeServiceCall(host, database);
            return null;
        }
    }

//    public class PostSynchronized extends AsyncTask<Void, Void, Void>{
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//            mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
//            operation = mSettings.getString("operation", "");
//            try {
//                httpHandler.postSynchData(operation);
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("База данных")
                .setContentText("Запущена")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

        return START_REDELIVER_INTENT;
    }
}