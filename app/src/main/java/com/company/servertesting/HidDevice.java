package com.company.servertesting;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

public class HidDevice {

    private Context context;
    private UsbManager manager;
    private UsbDevice device;
    private AtomicReference<UsbInterface> ifHid = new AtomicReference<>(null);
    private AtomicReference<UsbEndpoint> epIn = new AtomicReference<>(null);
    private UsbDeviceConnection connection;
    private PendingIntent pendingIntent;
    private Intent intent;
    private static String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    public HidDevice(final Context context, final UsbDevice device) throws IOException {
        this.context = context;
        this.device = device;
        intent = new Intent();
        intent.setClass(context.getApplicationContext(), ServerService.class);
        manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        pendingIntent = PendingIntent.getService(context.getApplicationContext(), 0, new Intent(ACTION_USB_PERMISSION), 0);

        if (!manager.hasPermission(device)){
            try {
                Log.i("USB","Request permission");
                manager.requestPermission(
                        device, PendingIntent.getService(
                                context.getApplicationContext(),
                                0,
                                new Intent("com.android.example.USB_PERMISSION"), 0
                        )
                );
            } catch (Exception ex) {ex.printStackTrace();}
        }

        new Thread(new Runnable() { // поток, который будет ждать разрешения
            @Override
            public void run() {

                while (!manager.hasPermission(device)) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                Log.i("USB","request open");
                connection = manager.openDevice(device);

                for (int i = 0; (ifHid.get() == null) && (i < device.getInterfaceCount()); i++)
                    if (device.getInterface(i).getInterfaceClass() == UsbConstants.USB_CLASS_HID) {
                        ifHid.set(device.getInterface(i));
                        for (int j = 0; j < ifHid.get().getEndpointCount(); j++) {
                            UsbEndpoint ep = ifHid.get().getEndpoint(j);
                            if ((ep.getDirection() == UsbConstants.USB_DIR_IN) && (ep.getType() == UsbConstants.USB_ENDPOINT_XFER_INT))
                                epIn.set(ep);
                        }
                    }
                if (ifHid.get() == null)
                    throw new IllegalArgumentException("Device has no HID interface");
                else if (epIn.get() == null)
                    throw new IllegalArgumentException("Device has no INTERRUPT IN endpoint (type USB_ENDPOINT_XFER_INT, direction USB_DIR_IN)");
                Log.i("USB","device connected");
            }
        }).start();

    }

    int x;
    public int read(final byte[] data, final int length) {
        if (connection != null && connection.claimInterface(ifHid.get(), true)){
            try{
                x = connection.bulkTransfer(epIn.get(), data, length, 10);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return x;
    }

}
