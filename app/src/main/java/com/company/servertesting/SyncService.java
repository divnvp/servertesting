package com.company.servertesting;

import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class SyncService extends TimerTask {

    public static final int SYNC_PERIOD = 15 * 1000;

    private HttpHandler httpHandler;
    private Timer timer = new Timer();

    public SyncService(HttpHandler httpHandler) {
        this.httpHandler = httpHandler;
    }

    @Override
    public void run() {
        Log.i("Sync Service", "sync...");
        /*
        В данном месте должен быть код, который извлекает записи из БД о операциях
        не прошедших синхронизацию. Далее данные операции отправляются на сервер,
        в ответ сервер присылает список id операций, которые прошли синхронизацию.
        По этин id операции в БД помечаются как синхронизованные.
         */
        try {
            // и лучше обернуть вся в try catch блок
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        timer.schedule(this,SYNC_PERIOD,SYNC_PERIOD);
    }

    public void stop() {
        timer.cancel();
    }
}
